import org.junit.*;

public class MyFirstTest {

    @BeforeClass

    public static void beforeClass(){
        System.out.println(" --> This runs before class");
    }
    @Before

    public void beforeTest(){
        System.out.println("- This runs before each test");
    }
    @Test

    public void Test01(){
        System.out.println("This is my first test");
    }

    @Ignore
    public void ingnoredTest(){
        System.out.println(" This is ignored ");
    }
    @Test
    public void Test02(){
        System.out.println( "Second test");}

        @After
    public void AfterTest(){
            System.out.println(" + This runs after test");
        }
        @AfterClass
    public static void afterClass(){
            System.out.println(" --> This runs after class");
        }
}
