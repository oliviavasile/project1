import curs7.Calculator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HomeworkCalculatorTest {
    static Calculator c;
    boolean result;


    @Before
    public void beforeTest ( ) {
        c = new Calculator();

    }

    @Test
    public void testSum01 ( ) {
        //Calculator c = new Calculator();
        //System.out.println(c.compute(3.1, 4.3, "+"));
        Assert.assertEquals(9 , c.compute(2 , 7 , "+") , 0);

    }

    @Test
    public void testSum02 ( ) {
        //Calculator c = new Calculator();
        Assert.assertEquals(114.7 , c.compute(215.28 , -100.58 , "+") , 0);
    }

    @Test
    public void testDifference01 ( ) {
        // Calculator c = new Calculator();
        Assert.assertEquals(900 , c.compute(1000 , 100 , "-") , 0);
    }

    @Test
    public void product01 ( ) {
        Assert.assertEquals(10 , c.compute(2 , 5 , "*") , 0);
    }

    @Test(expected = IllegalArgumentException.class)

    public void testUnsupported01 ( ) {
        Assert.assertEquals(121 , c.compute(11 , 11 , "x") , 0);
    }

    @Test()
    public void testDiv01 ( ) {
        Assert.assertEquals(2 , c.compute(10 , 5 , "/") , 0);
    }

    @Test()
    public void testDiv02 ( ) {
        Assert.assertEquals(3.33 , c.compute(10 , 3 , "/") , 0.1);
    }

    public void testSqrt ( ) {
        Assert.assertEquals(1.4142 , c.compute(2 , 0 , "SQRT") , 0.001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void divByZero01 ( ) {
        Assert.assertEquals(0 , c.compute(5 , 0 , "/") , 0);
    }

    //    @Test NU ESTE RECOMANDAT
//    public void divByZero02(){
//        try {
//            Assert.assertEquals(0 , c.compute(5 , 0 , "/") , 0);
//        }
//        catch (IllegalArgumentException e){
//            System.out.println(e.getMessage());
//        }
///DE AICI INCEPE TEMA
    @Test
    public void testSum03 ( ) {
        Assert.assertEquals(78 , c.compute(88 , -10 , "+") , 0);
    }

    @Test
    public void testSum04 ( ) {

        Assert.assertEquals(2 , c.compute(24.22 , -22 , "+") , 0.22);
    }

    @Test
    public void testSum05 ( ) {
        Assert.assertEquals(0 , c.compute(0 , 0 , "+") , 0);
    }

    @Test
    public void testDifference02 ( ) {
        Assert.assertEquals(-900 , c.compute(-1000 , -100 , "-") , 0);
    }

    @Test
    public void testDifference03 ( ) {
        Assert.assertEquals(-900 , c.compute(-1000.23 , -100.03 , "-") , 0.3);
    }

    @Test
    public void testDifference04 ( ) {
        Assert.assertEquals(-1100 , c.compute(-1000.23 , +100.03 , "-") , 0.26);
    }

    @Test
    public void product02 ( ) {
        Assert.assertEquals(0 , c.compute(0 , 0 , "*") , 0);
    }

    @Test
    public void product03 ( ) {
        Assert.assertEquals(-6 , c.compute(2 , -3 , "*") , 0);
    }

    @Test
    public void product04 ( ) {
        Assert.assertEquals(6 , c.compute(-2 , -3 , "*") , 0);
    }

    @Test(expected = IllegalArgumentException.class)

    public void testUnsupported02 ( ) {
        Assert.assertEquals(6 , c.compute(2 , 4 , "%") , 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void divByZero02 ( ) {
        Assert.assertEquals(0 , c.compute(1100 , 0 , "/") , 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void divByZero03 ( ) {
        Assert.assertEquals(0 , c.compute(0 , 0 , "/") , 0);
    }

    @Test
    public void testDiv03 ( ) {
        Assert.assertEquals(-2 , c.compute(-6 , 2 , "/") , 1);
    }

    @Test
    public void testSQRT01 ( ) {
        Assert.assertEquals(12 , c.compute(144 , 0 , "SQRT") , 0);
    }

    @Test
    public void testSQRT02 ( ) {
        Assert.assertEquals(4 , c.compute(17 , 0 , "SQRT") , 1);
    }

}

