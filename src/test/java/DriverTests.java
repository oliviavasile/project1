import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DriverTests {

    WebDriver driver;

    @Test

    public void myFirstDriverTest ( ) {
        System.setProperty("webdriver.chrome.driver" , "src/test/resources/drivers/chromedriver.exe");
        WebDriver driver = new ChromeDriver();//polimorfism chrome Driver este o clasa particulara care extends WebDriver
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("Koala" , Keys.ENTER);
        //searchInput.sendKeys(Keys.ENTER);
        //List<WebElement> titles = driver.findElements(By.xpath("*[@id=\"rso\"]/div[*]/div/div[1]/a/h3/span\n"));
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3 > span"));
        System.out.println("Numarul de rezultate este:" + titles.size());

        //xpath not reliable so smth else


        //driver.quit();// inchide tot browser-ul
        //driver.close();//inchide fereastra curenta, echivalent x dintr-o sg fereastra

    }

    @Test
    public void htmlUnitTest ( ) {
        WebDriver driver = new HtmlUnitDriver();
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("Koala" , Keys.ENTER);
        //searchInput.sendKeys(Keys.ENTER);

        System.out.println((driver.getTitle()));
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3 > span"));
        System.out.println("Numarul de rezultate este:" + titles.size());
        driver.quit();
    }

    @Test
    public void firefoxDriverTest ( ) {
        System.setProperty("webdriver.gecko.driver" , "src/test/resources/drivers/geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("Koala" , Keys.ENTER);
        //searchInput.sendKeys(Keys.ENTER);

        System.out.println((driver.getTitle()));
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3 > span"));
        System.out.println("Numarul de rezultate este:" + titles.size());
        driver.quit();

    }

    @Test
    public void edgeDriverTest ( ) {
        System.setProperty("webdriver.edge.driver" , "src/test/resources/drivers/msedgedriver.exe");
        WebDriver driver = new EdgeDriver();
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("Koala" , Keys.ENTER);
        //searchInput.sendKeys(Keys.ENTER);

        System.out.println((driver.getTitle()));
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3 > span"));
        System.out.println("Numarul de rezultate este:" + titles.size());
        driver.quit();

    }

//    @Test
//    public void IEDriverTest(){
//        System.setProperty("webdriver.ie.driver" , "src/test/resources/drivers/msedgedriver.exe");
//        WebDriver driver = new InternetExplorerDriver();
//        driver.get("http://www.google.com");
//        WebElement searchInput = driver.findElement(By.name("q"));
//        searchInput.sendKeys ( "Koala", Keys.ENTER);
//        //searchInput.sendKeys(Keys.ENTER);
//
//        System.out.println((driver.getTitle()));
//        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3 > span"));
//        System.out.println("Numarul de rezultate este:" + titles.size());
//        driver.quit();

    @Test
    public void driverManagerTest ( ) {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();//this shall be moved @ BEFORE
        WebDriver driver = new ChromeDriver();
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("Koala" , Keys.ENTER);
        //searchInput.sendKeys(Keys.ENTER);

        System.out.println((driver.getTitle()));
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3 > span"));
        System.out.println("Numarul de rezultate este:" + titles.size());
        driver.quit();////this shall be moved @ AFTER
    }

    @Test
    public void lazyButtonTest ( ) {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();//this shall be moved @ BEFORE
        driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/lazy.html");
        // driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
//    for( int i = 0; i < 5; i++){
//    WebElement lazyButton = driver.findElement(By.id("lazy-button"));
//    lazyButton.click();
//    }
        WebDriverWait wait = new WebDriverWait(driver , 15);
        for (int i = 0; i < 5; i++) {
            WebElement lazyButton = wait.until(
                    ExpectedConditions.presenceOfElementLocated(By.id("lazy-button")));
            lazyButton.click();
        }
        driver.quit();
    }

    @DataProvider(name="logindp")
    public Iterator<Object[]>loginDp(){

        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{ "", "", "Please enter your username", "Please enter your password", ""});

        dp.add(new String[]{ "aaaa", "", "", "Please enter your password", ""});

        dp.add(new String[]{ "", "aaaaa", "Please enter your username","", ""});

        dp.add(new String[]{ "aaaa", "aaaaa", "", "","Invalid username or password"});

        return dp.iterator();}

        @Test(dataProvider = "logindp")
                public void negativeLoginTest(String username, String password, String userErrMsg, String passErrMsg, String genErrMsg){
            WebDriverManager.getInstance(DriverManagerType.CHROME).setup();//this shall be moved @ BEFORE
            driver = new ChromeDriver();
            driver.get("http://86.121.249.149:4999/stubs/auth.html");
            WebElement usernameInput = driver.findElement(By.id("input-login-username"));
            WebElement passwordInput = driver.findElement(By.id("input-login-password"));
            WebElement submitButton = driver.findElement(By.id("login-submit"));
            usernameInput.clear();
            usernameInput.sendKeys(username);

            passwordInput.clear();
            passwordInput.sendKeys(password);
            submitButton.submit();

            WebElement err1 = driver.findElement(By.xpath("//*[@id=\"login_form\"]/div[2]/div/div[2]"));
            WebElement err2 = driver.findElement(By.xpath("//*[@id=\"login_form\"]/div[3]/div/div[2]"));
            WebElement err3 = driver.findElement(By.id("login-error"));

//        System.out.println(err1.getText());
//        System.out.println(err2.getText());
//        System.out.println(err3.getText());
            Assert.assertEquals(err1.getText(), userErrMsg);
            Assert.assertEquals(err2.getText(), passErrMsg);
            Assert.assertEquals(err3.getText(), genErrMsg);

            driver.close();

    }

}

