import curs7.Calculator;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@Test
public class MyFirstTestNGTest {
    Calculator c;

    @BeforeClass(groups = {"smoke"})
    public void setUp() {
        c = new Calculator();
    }

    @Test(description = "This is first testNG test", priority = 1, groups = {"smoke"})
    public void myTestMethod1() {
        System.out.println("This is a test method1");
    }

    public void myTestMethod2() {
        System.out.println("This is a test method 2");
    }

    private void myPrivateMethod() {
        System.out.println("This is NOT a test method");
    }

    @Test(description = "This is test methode to show priority")
    public void myTestMethod3() {
        System.out.println("This is a test method 3");
        this.myPrivateMethod();
    }

    @Test(groups = {"smoke"})
    public void testSum03() {
        Assert.assertEquals(1000, c.compute(1000, 0, "+"), 0);
    }

    @Test(dependsOnMethods = {"testSum03"})
    public void testDependsOnMethod() {
        System.out.println("---->Depends on method run");
    }

    @Test(dependsOnMethods = {"testSum03"}, alwaysRun = true, groups = {"smoke", "regression"})
    public void testDependsOnMethodAlwaysRun() {
        System.out.println("---->Always Run depends on method run");
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}, groups = {"smoke", "regression"})
    public void testException() {
        Assert.assertEquals(100, c.compute(1000, 0, ")('test"), 0);
    }

    @Test(groups = {"smoke"})
    @Parameters({"email", "password"})
    public void testLogin(String email, String password) {
        System.out.println("login with email:" + email + " and password:" + password);
    }
    @Test
    public void getParamFromCmd(){

    }
}
