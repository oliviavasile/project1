import curs7.Calculator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {

    static Calculator c;

    @Before
    public void beforeTest(){
        c = new Calculator();

    }

    @Test
    public void testSum01(){

        //System.out.println(c.compute(2,7,"+"));
        Assert.assertEquals(9, c.compute(2,7,"+"),0);
    }


    @Test
public void testSum02(){

        Assert.assertEquals(- 13482, c.compute(2164,-15646,"+"),0);
    }

    @Test
    public void testSum03(){


        Assert.assertEquals(125, c.compute(25,100,"+"),0);
    }

    @Test
    public void testDiff01(){

        Assert.assertEquals(- 75, c.compute(25,100,"-"),0);
    }

    @Test ( expected = IllegalArgumentException.class)
    public void TestUnsupp01(){

        Assert.assertEquals(- 75, c.compute(25,100,"x"),0);
    }
    @Test ()
    public void Div01(){

        Assert.assertEquals(2, c.compute(10,5,"/"),0);
    }

    @Test ()
    public void Div02(){

        Assert.assertEquals(3.33, c.compute(10,3,"/"),0.01);
    }

    @Test ()
    public void TestSqrt01(){

        Assert.assertEquals(1.4142, c.compute(2,0,"SQRT"),0.001);
    }

}
