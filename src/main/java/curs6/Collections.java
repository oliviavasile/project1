package curs6;

import java.util.*;

public class Collections {

    public static void ShowArray ( ) {
        int[] arrays = {1 , 2 , 4 , 5};
        System.out.println(arrays[2]);
        Car[] Cars = {new Truck() , new Truck()};

        //display entire array
        for (int i = 0; i <= arrays.length - 1; i++) {
            System.out.println(arrays[i]);
        }
        //display with forEach; cel recomandat

        for (int el : arrays) {
            System.out.println(el);
        }

        String[] nume = {"Ion" , "Maria" , "Vasile"};
        for (String el : nume) {
            System.out.println((el));
        }
    }

    public static void displayList (List list) {
        for (Object lst : list) {
            System.out.println(lst.toString());
        }
    }

    public static void showList ( ) {
        List arrayList = new ArrayList();
        arrayList.add(1);
        arrayList.add("Ana");
        arrayList.add("Ana are mere");
        arrayList.add(2);
        displayList(arrayList);

        arrayList.remove("Ana");
        displayList(arrayList);
    List<String> names = new ArrayList<String>();
    names.add("ion");
    names.add("vasile");
    System.out.println(names.size() + names.toString());
        System.out.println( names.get(1));
        Set<Integer> setList = new HashSet<Integer>();
        setList.add(1);
        setList.add(2);
        setList.add(3);
        setList.add(3);

        System.out.println(setList.toString());

    }

    public static void displayMap(Map hashMap){

        for( Object key: hashMap.keySet()){
            System.out.println(key + "" + hashMap.get(key));
        }

    }
public static void showHashMap(){
    Map<String, Integer> hashMap = new HashMap<String, Integer>();
    hashMap.put("laptop",3);
    hashMap.put("pc",5);
    hashMap.put("pc",2);
    hashMap.put("mobile",4);
    displayMap(hashMap);

    //display

}

        public static void main (String[]args){
           //showList();
            showHashMap();

        }
    }


