package ReproducereCurs;

public class Draw {
    public static void main(String[] args) {
        drawFullShape(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeOutLine(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeCorners(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawFullShape(Integer.parseInt(args[0]));
    }

    public static void drawShapeCorners(int weight, int height) {
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < weight; i++) {
                if (j == 0 || j == height - 1) {
                    if (i == 0 || i == weight - 1) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                } else {
                    System.out.println(" ");
                }
            }
            System.out.println();

        }
    }

    public static void drawShapeOutLine(int weight, int height) {
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < weight; i++) {
                if (i == 0 || j == height - 1) { // daca suntem pe primasau ultima linie afisam numai stelute
                    System.out.print("*");
                } else { // daca nu suntem pe ultima linie
                    if (i == 0 || i == weight - 1) { //daca suntem pe primul sau ultimul caracter de pe linie
                        System.out.print("*");
                    } else { // daca nu suntem pe ultimul sau primul caracter
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }

    public static void drawFullShape(int weight, int height) {
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < weight; i++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void drawFullShape (int l) {
        drawFullShape(l, l);

    }
}


