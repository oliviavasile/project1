package ReproducereCurs;

public class Square {
    double squareSide;

    void setSide(double s){
        squareSide = s;
    }
    double getArea(){
        return squareSide * squareSide;
    }
    void printSquare() {
        System.out.println(" Avem un patrat cu latura " + squareSide + " si cu aria "  + getArea());
    }
}
